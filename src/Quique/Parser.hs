{-# language LambdaCase #-}
{-# language PatternSynonyms #-}
module Quique.Parser (
  parseProgram
) where

import Control.Monad (guard)
import Data.Char (isUpper, isLower)
import CHR.Typed (pattern T, pattern V, pattern NmV, U)
import Text.Parsec
import Text.Parsec.Language
import qualified Text.Parsec.Token as T
import Unbound.Generics.LocallyNameless

import Quique.Types
import Quique.Terms

parseExpr :: Parsec String s UExpr
parseExpr = parseExpr_ `chainl1` pure UApp

parseExpr_ :: Parsec String s UExpr
parseExpr_ = parens ( try ((\e (T t) -> UAnn e t)
                             <$> parseExpr
                             <*  reservedOp "::"
                             <*> parseTy)
                    {- <|> try (UTyApp <$> parseExpr
                                       <*> braces parseTy) -}
                    <|> parseExpr )
         <|> UInt <$> integer
         <|> UStr <$> stringLiteral
         <|> try ((\(x, T t) e -> UAnnLam (bind (s2n x, embed t) e))
                        <$  reservedOp "\\"
                        <*> parens ((,) <$> identifier
                                        <*  reservedOp "::"
                                        <*> parseTy)
                        <*  reservedOp "->"
                        <*> parseExpr)
         <|> (\x e -> ULam (bind (s2n x) e))
                        <$  reservedOp "\\"
                        <*> identifier
                        <*  reservedOp "->"
                        <*> parseExpr
         <|> try ((\x (T t) e1 e2 -> UAnnLet (bind (s2n x, embed t, embed e1) e2))
                        <$  reserved "let"
                        <*> identifier
                        <*  reservedOp "::"
                        <*> parseTy
                        <*  reservedOp "="
                        <*> parseExpr
                        <*  reserved "in"
                        <*> parseExpr)
         <|> (\x e1 e2 -> ULet (bind (s2n x, embed e1) e2))
                        <$  reserved "let"
                        <*> identifier
                        <*  reservedOp "="
                        <*> parseExpr
                        <*  reserved "in"
                        <*> parseExpr
         <|> UVar <$> (s2n <$> identifier)

parseTy :: Parsec String s (U Ty)
parseTy = foldr1 (\x y -> T (Ty_Arrow' x y)) <$> parseTy_ `sepBy1` reservedOp "->"

parseTy_ :: Parsec String s (U Ty)
parseTy_ = parens parseTy
       <|> try (V <$> parseMetaVarNumeric)
       <|> NmV <$> parseMetaVarNamed
       <|> T <$> (   Ty_List' <$> brackets parseTy
                 <|> try (buildTyForAll
                      <$  reserved "forall"
                      <*> many identifier
                      <*  reservedOp "."
                      <*> parens (commaSep1 parseCon)
                      <*  reservedOp "=>"
                      <*> parseTy)
                 <|> buildTyForAll
                      <$  reserved "forall"
                      <*> many identifier
                      <*  reservedOp "."
                      <*> pure []
                      <*> parseTy
                 <|> Ty_Fam <$> (T <$> parseFamilyName)
                      <*> (T <$> parseTyList)
                 <|> Ty_Con <$> (T <$> parseDataName)
                      <*> (T <$> parseTyList)
                 <|> Ty_Var <$> (T <$> (s2n <$> parseVarName)) )
  where buildTyForAll x c1 (TTy_ForAll (T b))
          = runFreshM $ do (v2, (c2, ty)) <- unbind b
                           let v1 = map s2n x
                           return $ Ty_ForAll (T (bind (v1 ++ v2) (c1 ++ c2, ty)))
        buildTyForAll x c (T t) = Ty_ForAll (T (bind (map s2n x) (c, t)))

parseTyList :: Parsec String s [U Ty]
parseTyList = many (    parens parseTy
                    <|> try (V <$> parseMetaVarNumeric)
                    <|> NmV <$> parseMetaVarNamed
                    <|> T <$> (   (\x -> Ty_Fam (T x) (T [])) <$> parseFamilyName
                              <|> (\x -> Ty_Con (T x) (T [])) <$> parseDataName
                              <|> Ty_List' <$> brackets parseTy
                              <|> Ty_Var   <$> (T <$> (s2n <$> parseVarName)) ) )

parseDataName :: Parsec String s String
parseDataName = try $ do r@(f:_) <- identifier
                         guard (isUpper f)
                         return r

parseFamilyName :: Parsec String s String
parseFamilyName = try (id <$ char '!' <*> parseDataName)

parseVarName :: Parsec String s String
parseVarName = try $ do r@(f:_) <- identifier
                        guard (isLower f)
                        return r

parseMetaVarNamed :: Parsec String s String
parseMetaVarNamed = try (id <$ char '$' <*> identifier)

parseMetaVarNumeric :: Parsec String s Int
parseMetaVarNumeric = try (fromInteger <$ char '$' <*> natural)

parseCon :: Parsec String s Con
parseCon = try (Con_Eq <$> parseTy
                       <*  reservedOp "~"
                       <*> parseTy)
       <|> try ((\t1 t2 -> Con_Inst t1 (T []) (T Result_Mono) t2)
                       <$> parseTy
                       <*  reservedOp "<"
                       <*> parseTy)
       <|> (\n l -> Con_Class (T n) (T l))
                       <$> parseDataName
                       <*> parseTyList

parseAxiom :: Parsec String s Axiom
parseAxiom = try (Axiom_ConstraintImpl
                    <$> parseCon
                    <*  reservedOp "=>"
                    <*> commaSep1 parseCon)
         <|> try (Axiom_ConstraintIff
                    <$> parseCon
                    <*  reservedOp "<=>"
                    <*> commaSep1 parseCon)
         <|> (\(T t1) (T t2) -> Axiom_TypeRewriting t1 t2)
                 <$> parseTy
                 <*  reservedOp "~"
                 <*> parseTy

parseBucket :: Parsec String s Bucket
parseBucket = try (Bucket_Bucket
                         <$  reserved "bucket"
                         <*> stringLiteral
                         <*> braces (commaSep1 parseBucket))
          <|> try (Bucket_Arg
                         <$  reserved "argument"
                         <*> (s2n <$> identifier))
          <|> Bucket_Leaf <$> parseCon

parseDecl :: Parsec String s Decl
parseDecl = try (Bucket  <$  reserved "rule"
                         <*> (s2n <$> identifier)
                         <*> many (s2n <$> identifier)
                         <*  reservedOp "::"
                         <*> commaSep1 parseBucket
                         <*  reservedOp ";")
        <|> try (Import  <$> (s2n <$> identifier)
                         <*  reservedOp "::"
                         <*> (unT <$> parseTy)
                         <*  reservedOp ";")
        <|> try (Axiom   <$  reserved "axiom"
                         <*> parseAxiom
                         <*  reservedOp ";")
        <|> try (Typed   <$> (s2n <$> identifier)
                         <*  reservedOp "::"
                         <*> (unT <$> parseTy)
                         <*  reservedOp "="
                         <*> parseExpr
                         <*  reservedOp ";")
        <|>      Untyped <$> (s2n <$> identifier)
                         <*  reservedOp "="
                         <*> parseExpr
                         <*  reservedOp ";"
  where unT (T t) = t
        unT _     = error "Needs a closed type"
                   

parseProgram :: Parsec String s Program
parseProgram = many parseDecl

--- The lexer ---
lexer :: T.TokenParser t
lexer = T.makeTokenParser haskellDef
parens :: Parsec String s a -> Parsec String s a
parens = T.parens lexer
braces :: Parsec String s a -> Parsec String s a
braces = T.braces lexer
brackets :: Parsec String s a -> Parsec String s a
brackets = T.brackets lexer
identifier :: Parsec String s String
identifier = T.identifier lexer
reserved :: String -> Parsec String s ()
reserved = T.reservedOp lexer
reservedOp :: String -> Parsec String s ()
reservedOp = T.reservedOp lexer
integer :: Parsec String s Integer
integer = T.integer lexer
natural :: Parsec String s Integer
natural = T.natural lexer
stringLiteral :: Parsec String s String
stringLiteral = T.stringLiteral lexer
commaSep1 :: Parsec String s a -> Parsec String s [a]
commaSep1 = T.commaSep1 lexer
