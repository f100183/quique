{-# language ViewPatterns #-}
{-# language PatternSynonyms #-}
{-# language TupleSections #-}
module Quique.Generation (
  reconstructProgram
) where

import CHR.Typed (pattern T, RuleVar(..), U)
import Control.Monad (forM)
import Data.Bifunctor (second)
import Data.Dynamic
import Data.List
import Data.Maybe (catMaybes, fromMaybe)
import Data.TermWithVars (Substitution, substs)
import Unbound.Generics.LocallyNameless hiding (substs)
import Unbound.Generics.LocallyNameless.Name

import Quique.Types
import Quique.Terms

type Env = [(UVar, Ty)]
type Buckets = [Decl]
type TypeCheckedDecl = (UVar, TExpr, ErrorConSet, [ErrorInfo], MonoVarsInfo)

reconstructProgram :: Fresh m => Program -> m ([Axiom], [TypeCheckedDecl])
reconstructProgram p
  = let (env, axioms, buckets, rest) = splitProgram p
     in (axioms,) <$> mapM (reconstructDecl buckets env) rest

splitProgram :: Program -> (Env, [Axiom], Buckets, [Decl])
splitProgram p
  = let (toTypeCheck, rest) = partition needsTypechecking p
        imports = filter isImport rest
        axioms  = filter isAxiom  rest
        buckets = filter isBucket rest
        env = [(v, t) | Import v t <- imports]
        axs = [ax | Axiom ax <- axioms]
    in (env, axs, buckets, toTypeCheck)

reconstructDecl :: Fresh m => Buckets -> Env
                -> Decl -> m TypeCheckedDecl
reconstructDecl b env (Untyped v e)
  = do initialBucket <- fresh (s2n "root")
       (e', c, einfo, ng, _) <- reconstruct b env initialBucket e
       return (v, e', c, einfo, ng)
reconstructDecl b env (Typed v t e)
  = do initialBucket <- fresh (s2n "root")
       (e', c, einfo, ng, _) <- reconstruct b env initialBucket (UAnn e t)
       return (v, e', c, einfo, ng)
reconstructDecl _ _ _
  = error "Imports should not go here!"

reconstructHead :: Fresh m => Buckets -> Env
                -> ErrorContextIx -> UExpr
                -> m (TExpr, ErrorConSet, [ErrorInfo], MonoVarsInfo)
reconstructHead bus env ix e
  | ((Bucket v nargs b, args):_) <- catMaybes (map (match_bucket_rule e) bus)
  = -- If a rule matches, use it
    reconstruct' bus env ix e
reconstructHead bus env ix (UVar v)
  = do let Just ty = lookup v env
       return ( (Var (translate v), ty), [], [], mempty )
reconstructHead bus env ix e
  = -- If it is not a var, just apply the normal stuff
    reconstruct' bus env ix e

reconstruct' :: Fresh m => Buckets -> Env
             -> ErrorContextIx -> UExpr
             -> m (TExpr, ErrorConSet, [ErrorInfo], MonoVarsInfo)
reconstruct' bus env ix e
  = do (t, e, info, v, _) <- reconstruct bus env ix e
       return (t, e, info, v)

reconstruct :: Fresh m => Buckets -> Env
            -> ErrorContextIx -> UExpr
            -> m (TExpr, ErrorConSet, [ErrorInfo], MonoVarsInfo, Int)
reconstruct bus env ix e
  | ((Bucket v nargs b, args):_) <- catMaybes (map (match_bucket_rule e) bus)
  = do retVar <- Ty_Var . T <$> fresh (s2n "t")
       (argsR, cs, einfo, ng) <- go_around_buckets bus env ix b nargs args
       newSubst <- build_bucket_substitution retVar nargs (map snd argsR)
       return ( (Rule (translate v) argsR, retVar)
              , substs newSubst cs, einfo, ng, 0 )
{- TODO: CHANGE COMPLETELY
reconstruct bus env ix (UAnn e sigma)
  = do (e'@(_, phi), c, einfo, ng) <- reconstruct bus env ix e
       let gamma  = nub (fvs (phi, c)) \\ nub (fvs (env, sigma))
           instCo = inst gamma ng c phi 0 sigma
       return ( (Ann e' sigma, sigma)
              , [(instCo, ix)], einfo, liftToMonoVarsInfo (\\ gamma) ng)
-}
reconstruct bus env ix (ULam b)
  = do (x, e) <- unbind b
       alpha <- fresh (s2n "t")
       let alphaTy = Ty_Var (T alpha)
       (e'@(_, sigma), c, einfo, ng, n) <- reconstruct bus ((x, alphaTy):env) ix e
       return ( (Lam (bind (translate x) e'), Ty_Arrow alphaTy sigma)
              , c, einfo, ng `plusFullyMono` alpha, n+1 )
reconstruct bus env ix (UAnnLam b)
  = do ((x, unembed -> phi), e) <- unbind b
       (e'@(_, sigma), c, einfo, ng, n) <- reconstruct bus ((x, phi):env) ix e
       return ( (AnnLam (bind (translate x, embed phi) e'), Ty_Arrow phi sigma)
              , c, einfo, ng, n+1 )
{-
reconstruct env (UTyApp e phi)
  = ...  -- long time gone  --  ...
-}
reconstruct _ _ _ (UInt n)
  = return ((Int n, Ty_Int), [], [], mempty, 0)
reconstruct _ _ _ (UStr s)
  = return ((Str s, Ty_String), [], [], mempty, 0)
reconstruct bus env ix (ULet b)
  = do ((x, unembed -> e1), e2) <- unbind b
       (e1'@(_,phi),   c1, e1, ng1, _)  <- reconstruct bus env ix e1
       (e2'@(_,sigma), c2, e2, ng2, n2) <- reconstruct bus ((x,phi):env) ix e2
       return ( (Let (bind (translate x, embed e1') e2'), sigma)
              , c1 ++ c2, e1 ++ e2, ng1 ++ ng2, n2 )
reconstruct bus env ix (UAnnLet b)
  = do ((x, unembed -> phi, unembed -> e1), e2) <- unbind b
       (e1', c1, e1, ng1, _) <- reconstruct bus env ix (UAnn e1 phi)
       (e2'@(_,sigma), c2, e2, ng2, n2) <- reconstruct bus ((x,phi):env) ix e2
       return ( (AnnLet (bind (translate x, embed phi, embed e1') e2'), sigma)
              , c1 ++ c2, e1 ++ e2, ng1 ++ ng2, n2 )

-- The complex case
reconstruct bus env ix e
  = do let (f, args) = splitNAryApp bus e
       -- Reconstruct the head
       (f'@(_,sigma), c, einfo, ng) <- reconstructHead bus env ix f
       -- Reconstruct the arguments
       argsR <- mapM (reconstruct bus env ix) args
       let (args', _, einfos, _, _) = unzip5 argsR
       -- Build the constraints
       (argsCos, argsNg, argsRet)
         <- reconstructApp (length args) sigma (nub (fvs (env, sigma, c))) argsR
       delta <- fresh (s2n "d")
       let deltaTy = Ty_Var (T delta)
           lastInst = inst argsRet 0 deltaTy
       return ( (NApp f' args', deltaTy)
              , c ++ map (,ix) (lastInst:argsCos), einfo ++ concat einfos
              , ng `mappend` argsNg `plusTopLevelMono` delta )
  where
    reconstructApp :: Fresh m => [UseGuardedness] -> Ty -> [TyVar]
                   -> [(TExpr, ErrorConSet, a, MonoVarsInfo)]
                   -> m ([Con], MonoVarsInfo, Ty)
    reconstructApp _ current_f env_vars []
      = return ([], mempty, current_f)  -- (Constraints, OpenVars, ReturnType)
    reconstructApp n current_f env_vars (((_,sigma),c,_,ng):rs)
      = do -- Build for this iterations
           alpha <- fresh (s2n "a")
           beta  <- fresh (s2n "b")
           let (alphaTy, betaTy) = (Ty_Var (T alpha), Ty_Var (T beta))
               gamma  = nub (fvs (sigma, c)) \\ env_vars
               instCo = inst current_f n (Ty_Arrow alphaTy betaTy)
               genCo  = gen gamma ng c sigma 0 alphaTy
               newNg  = liftToMonoVarsInfo (\\ gamma) ng
           -- Move on to next iterations
           (cs, ngs, ret) <- reconstructApp (n-1) betaTy env_vars rs
           -- Put everything together
           return (instCo:genCo:cs, newNg `mappend` ngs, ret)

-- Split a sequence of applications into head + arguments
splitNAryApp :: Buckets -> UExpr -> (UExpr, [UExpr])
splitNAryApp bus e
  | ((Bucket v nargs b, args):_) <- catMaybes (map (match_bucket_rule e) bus)
  = (e, [])
splitNAryApp bus (UApp f e)
  = second (++ [e]) (splitNAryApp bus f)
splitNAryApp bus e
  = (e, [])

match_bucket_rule :: UExpr -> Decl -> Maybe (Decl, [UExpr])
match_bucket_rule (UVar v)   b@(Bucket v' [] d)
  | v == v' = Just (b, [])
match_bucket_rule (UApp f e) b@(Bucket v' (_:as) d)
  = case match_bucket_rule f (Bucket v' as d) of
      Nothing        -> Nothing
      Just (_, args) -> Just (b, args ++ [e])
match_bucket_rule _ _ = Nothing

go_around_buckets :: Fresh m => Buckets -> Env -> ErrorContextIx
                  -> [Bucket] -> [UVar] -> [UExpr]
                  -> m ([TExpr], ErrorConSet, [ErrorInfo], MonoVarsInfo)
go_around_buckets bus env ix bs nargs args
  = do (iargs', cs, einfo, ng) <- go_around_buckets' bus env ix bs nargs args
       let args' = [ fromMaybe (error ("cannot find " ++ show n)) (lookup n iargs') | n <- nargs ]
       return (args', cs, einfo, ng)

go_around_buckets' :: Fresh m => Buckets -> Env -> ErrorContextIx
                   -> [Bucket] -> [UVar] -> [UExpr]
                   -> m ([(UVar,TExpr)], ErrorConSet, [ErrorInfo], MonoVarsInfo)
go_around_buckets' _ _ _ [] _ _
  = return ([], [], [], mempty)
go_around_buckets' bus env ix (b:bs) nargs args
  = do (o,  c,  i,  ng ) <- go_around_bucket'  bus env ix b  nargs args
       (os, cs, is, ngs) <- go_around_buckets' bus env ix bs nargs args
       return (o ++ os, c ++ cs, i ++ is, ng `mappend` ngs)

go_around_bucket' :: Fresh m => Buckets -> Env -> ErrorContextIx
                  -> Bucket -> [UVar] -> [UExpr]
                  -> m ([(UVar,TExpr)], ErrorConSet, [ErrorInfo], MonoVarsInfo)
go_around_bucket' _ _ ix (Bucket_Leaf c) _ _
  = return ([], [(c, ix)], [], mempty)
go_around_bucket' bus env ix (Bucket_Arg i) nargs args
  = do let place = fromMaybe (error ("cannot find argument " ++ show i)) (elemIndex i nargs)
       (expr, cs, einfo, ng) <- reconstruct' bus env ix (args !! place)
       return ([(i, expr)], cs, einfo, ng)
go_around_bucket' bus env ix (Bucket_Bucket msg bs) nargs args
  = do newBucket <- fresh (s2n "bu")
       (newArgs, cs, einfo, ng) <- go_around_buckets' bus env newBucket bs nargs args
       return (newArgs, cs, (newBucket, ix, msg) : einfo, ng)

build_bucket_substitution :: Fresh m => Ty -> [UVar] -> [Ty] -> m [(RuleVar, Dynamic)]
build_bucket_substitution retVar argNames argTys
  = do let nUMBER_OF_VARS = 30
       newVars <- forM [0 .. nUMBER_OF_VARS] (\n -> Ty_Var . T <$> fresh (s2n ("v" ++ show n)))
       return $ (NamedRuleVar "return", toDyn (T retVar :: U Ty))
                :  (zipWith (\n t -> (NamedRuleVar (name2String n), toDyn (T t :: U Ty))) argNames argTys)
                ++ (zipWith (\n t -> (RuleVar n, toDyn (T t :: U Ty))) [0 .. nUMBER_OF_VARS] newVars)

translate :: Name a -> Name b
translate (Fn s n) = Fn s n
translate (Bn s n) = Bn s n

inst :: Ty -> [UseGuardedness] -> ResultGuardedness -> Ty -> Con
inst ty1 g rg ty2 = Con_Inst (T ty1) (T g) (T rg) (T ty2)

gen :: [TyVar] -> MonoVarsInfo -> ErrorConSet -> Ty -> Int -> ResultGuardedness -> Ty -> Con
gen vs ng cs ty1 g rg ty2
  = Con_Gen (T (GenTy_Vars (T (bind vs (liftToMonoVarsInfo (`intersect` vs) ng, cs, ty1)))))
            (T g) (T rg) (T ty2)