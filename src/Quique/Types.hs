{-# language MultiParamTypeClasses #-}
{-# language TypeSynonymInstances #-}
{-# language FlexibleInstances #-}
{-# language FlexibleContexts #-}
{-# language DeriveGeneric #-}
{-# language PatternSynonyms #-}
module Quique.Types where

import CHR.Typed
import Control.Applicative
import Data.Bifunctor (first)
import Data.List (nub, (\\))
import qualified Data.TermWithVars as Tm
import GHC.Generics
import Unbound.Generics.LocallyNameless
import Unbound.Generics.LocallyNameless.Internal.Fold (toListOf)

fvs :: Alpha a => a -> [TyVar]
fvs = toListOf fv

type TyVar = Name Ty

data Ty = Ty_Var (U TyVar)
        | Ty_Con (U String) (U [U Ty])
        | Ty_Fam (U String) (U [U Ty])
        | Ty_ForAll (U (Bind [TyVar] ([Con], Ty)))
        deriving (Show, Generic)

pattern TTy_Var v    = T (Ty_Var v)
pattern TTy_Con c a  = T (Ty_Con c a)
pattern TTy_Fam f a  = T (Ty_Fam f a)
pattern TTy_ForAll b = T (Ty_ForAll b)

pattern Ty_Arrow  s t = Ty_Con (T "->") (T [T s, T t])
pattern Ty_Arrow' s t = Ty_Con (T "->") (T [s, t])
pattern Ty_List  e    = Ty_Con (T "List") (T [T e])
pattern Ty_List'  e   = Ty_Con (T "List") (T [e])
pattern Ty_Int        = Ty_Con (T "Int") (T [])
pattern Ty_Char       = Ty_Con (T "Char") (T [])
pattern Ty_String     = Ty_List Ty_Char

mkTyArrow :: Ty -> Ty -> Ty
mkTyArrow s t = Ty_Con (T "->") (T [T s, T t])

splitArrow :: Int -> U Ty -> ([U Ty], U Ty)
splitArrow 0 e = ([], e)
splitArrow n (T (Ty_Arrow' s t))
  = first (s:) (splitArrow (n-1) t)
splitArrow _ e = ([], e)

deepSkol :: Fresh m => Int -> Ty -> m (Maybe ([TyVar], [Con], Ty))
deepSkol 0 e = return $ Just ([], [], e)
deepSkol n (Ty_Arrow s t)
  = do maybeT <- deepSkol (n-1) t
       case maybeT of
         Nothing -> return Nothing
         Just (vars', cons', t') -> return $ Just (vars', cons', mkTyArrow s t')
deepSkol n (Ty_ForAll (T b))
  = do (vars, (cons, t)) <- unbind b
       maybeT <- deepSkol n t
       case maybeT of
         Nothing -> return Nothing
         Just (vars', cons', t') -> return $ Just (vars ++ vars', cons ++ cons', t')
deepSkol n e = return $ Just ([], [], e)

isFullyMono :: U Ty -> Bool
isFullyMono (TTy_Var _)
  = True
isFullyMono (TTy_Con _ (T args))
  = all isFullyMono args
isFullyMono (TTy_Fam _ (T args))
  = all isFullyMono args
isFullyMono (TTy_ForAll _)
  = False
isFullyMono _ = True

data ErrorContext -- we do not really need it, only its names
type ErrorMsg       = String
type ErrorContextIx = Name ErrorContext
type ErrorInfo      = (ErrorContextIx, ErrorContextIx, ErrorMsg)
type ErrorConSet    = [(Con, ErrorContextIx)]

data MonoVarsInfo = MonoVarsInfo { fullyMono    :: [TyVar]
                                 , topLevelMono :: [TyVar] }
                  deriving (Show, Generic)

instance Monoid MonoVarsInfo where
  mempty = MonoVarsInfo [] []
  mappend (MonoVarsInfo f1 t1) (MonoVarsInfo f2 t2)
         = let f' = nub (f1 ++ f2)
               t' = nub (t1 ++ t2) Data.List.\\ f'
           in MonoVarsInfo f' t'

plusFullyMono :: MonoVarsInfo -> TyVar -> MonoVarsInfo
plusFullyMono (MonoVarsInfo f t) v = MonoVarsInfo (nub (v:f)) (t Data.List.\\ [v])
plusTopLevelMono :: MonoVarsInfo -> TyVar -> MonoVarsInfo
plusTopLevelMono (MonoVarsInfo f t) v = MonoVarsInfo f (nub (v:t))

unionFullyMono :: MonoVarsInfo -> [TyVar] -> MonoVarsInfo
unionFullyMono (MonoVarsInfo f t) v = MonoVarsInfo (nub (f ++ v)) (t Data.List.\\ v)

isTopLevelMonoIn :: TyVar -> MonoVarsInfo -> Bool
isTopLevelMonoIn v (MonoVarsInfo f t) = v `elem` f || v `elem` t

liftToMonoVarsInfo :: ([TyVar] -> [TyVar])
                   -> MonoVarsInfo -> MonoVarsInfo
liftToMonoVarsInfo d (MonoVarsInfo f t)
  = MonoVarsInfo (d f) (d t)
lift2ToMonoVarsInfo :: ([TyVar] -> [TyVar] -> [TyVar])
                    -> MonoVarsInfo -> MonoVarsInfo -> MonoVarsInfo
lift2ToMonoVarsInfo d (MonoVarsInfo f1 t1) (MonoVarsInfo f2 t2)
  = MonoVarsInfo (d f1 f2) (d t1 t2)

data GenTy = GenTy_Vars (U (Bind [TyVar] (MonoVarsInfo, ErrorConSet, Ty)))
           deriving (Show, Generic)

data UseGuardedness = AllMono | UseGuard
                    deriving (Show, Generic)
data ResultGuardedness = Result_Mono | Result_Unrestricted
                       deriving (Show, Generic)

pattern TGenTy_Vars  b = T (GenTy_Vars b)

data Con = Con_Eq    (U Ty) (U Ty)
         | Con_Class (U String) (U [U Ty])
         | Con_Err   (U String)
         | Con_Inst  (U Ty) (U [UseGuardedness]) (U ResultGuardedness) (U Ty)
         | Con_Gen   (U GenTy) (U Int) (U ResultGuardedness) (U Ty)
         deriving (Show, Generic)

pattern TCon_Eq    x y     = T (Con_Eq x y)
pattern TCon_Class c a     = T (Con_Class c a)
pattern TCon_Err   s       = T (Con_Err s)
pattern TCon_Inst  x n r y = T (Con_Inst x n r y)
pattern TCon_Gen   x n r y = T (Con_Gen  x n r y)

data Axiom = Axiom_ConstraintImpl Con [Con]
           | Axiom_ConstraintIff  Con [Con]
           | Axiom_TypeRewriting  Ty  Ty
           deriving (Show, Generic)

instance Alpha RuleVar
instance Alpha t => Alpha (U t)
instance Subst e RuleVar
instance Subst e t => Subst e (U t)

instance Tm.Substitutable RuleVar (Name t)
instance Tm.Unifiable RuleVar     (Name t)

instance (Alpha v, Alpha t, Tm.Substitutable RuleVar t) => Tm.Substitutable RuleVar (Bind v t) where
  subst v e b = runFreshM $ do (vs, tm) <- unbind b
                               return (bind vs (Tm.subst v e tm))

instance (Alpha v, Alpha t, Tm.Unifiable RuleVar t) => Tm.Unifiable RuleVar (Bind v t) where
  unify t s | t `aeq` s = pure Tm.emptySubstitution
            | otherwise = empty

instance Alpha Ty
instance Subst Ty Ty where
  isvar (Ty_Var (T v)) = Just (SubstName v)
  isvar _              = Nothing
instance Tm.Substitutable RuleVar Ty
instance Tm.Unifiable RuleVar     Ty

instance Alpha MonoVarsInfo
instance Subst Ty MonoVarsInfo
instance Tm.Substitutable RuleVar MonoVarsInfo
instance Tm.Unifiable RuleVar     MonoVarsInfo

instance Alpha GenTy
instance Subst Ty GenTy
instance Tm.Substitutable RuleVar GenTy
instance Tm.Unifiable RuleVar     GenTy

instance Alpha ResultGuardedness
instance Subst Ty ResultGuardedness
instance Tm.Substitutable RuleVar ResultGuardedness
instance Tm.Unifiable RuleVar     ResultGuardedness

instance Alpha UseGuardedness
instance Subst Ty UseGuardedness
instance Tm.Substitutable RuleVar UseGuardedness
instance Tm.Unifiable RuleVar     UseGuardedness

instance Alpha Con
instance Subst Ty Con
instance Tm.Substitutable RuleVar Con
instance Tm.Unifiable RuleVar     Con

instance Alpha Axiom
instance Subst Ty Axiom
instance Tm.Substitutable RuleVar Axiom
instance Tm.Unifiable RuleVar     Axiom