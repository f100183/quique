{-# language MultiParamTypeClasses #-}
{-# language FlexibleInstances #-}
{-# language DeriveGeneric #-}
{-# language PatternSynonyms #-}
module Quique.Terms where

import Data.Typeable (Typeable)
import GHC.Generics
import Unbound.Generics.LocallyNameless

import Quique.Types

type Var ann = Name (Expr ann)
type AnnExpr ann = (Expr ann, ann)
data Expr ann = Var    (Var ann)
              | Ann    (AnnExpr ann) Ty
              | App    (AnnExpr ann) (AnnExpr ann)  -- only for parsing
              | NApp   (AnnExpr ann) [AnnExpr ann]  -- only for type checking
              | Lam    (Bind (Var ann) (AnnExpr ann))
              | AnnLam (Bind (Var ann, Embed Ty) (AnnExpr ann))
              -- | TyApp  (AnnExpr ann) Ty
              | Int    Integer
              | Str    String
              | Let    (Bind (Var ann,           Embed (AnnExpr ann)) (AnnExpr ann))
              | AnnLet (Bind (Var ann, Embed Ty, Embed (AnnExpr ann)) (AnnExpr ann))
              | Rule   (Var ann) [AnnExpr ann]
              deriving (Show, Generic)

type UVar  = Var  ()
type UExpr = AnnExpr ()
pattern UVar v     = (Var v,     ())
pattern UAnn e  t  = (Ann e  t,  ())
pattern UApp e1 e2 = (App e1 e2, ())
pattern UNApp f a  = (NApp f a,  ())
pattern ULam b     = (Lam b,     ())
pattern UAnnLam b  = (AnnLam b,  ())
-- pattern UTyApp e t = (TyApp e t, ())
pattern UInt n     = (Int n,     ())
pattern UStr s     = (Str s,     ())
pattern ULet b     = (Let b,     ())
pattern UAnnLet b  = (AnnLet b,  ())

type TVar  = Var  Ty
type TExpr = AnnExpr Ty

instance (Alpha ann, Typeable ann) => Alpha (Expr ann)
instance Subst (Expr ()) (Expr ()) where
  isvar (Var v) = Just (SubstName v)
  isvar _       = Nothing
instance Subst Ty (Expr ()) where
instance Subst (Expr ()) Ty where
instance Subst (Expr ()) MonoVarsInfo where
instance Subst (Expr ()) GenTy where
instance Subst (Expr ()) UseGuardedness where
instance Subst (Expr ()) ResultGuardedness where
instance Subst (Expr ()) Con where
instance Subst (Expr Ty) (Expr Ty) where
  isvar (Var v) = Just (SubstName v)
  isvar _       = Nothing
instance Subst Ty (Expr Ty) where
instance Subst (Expr Ty) Ty where
instance Subst (Expr Ty) MonoVarsInfo where
instance Subst (Expr Ty) GenTy where
instance Subst (Expr Ty) UseGuardedness where
instance Subst (Expr Ty) ResultGuardedness where
instance Subst (Expr Ty) Con where

type Program = [Decl]
data Decl = Untyped UVar UExpr
          | Typed   UVar Ty UExpr
          | Import  UVar Ty
          | Axiom   Axiom
          | Bucket  UVar [UVar] [Bucket]
          deriving (Show, Generic)

needsTypechecking :: Decl -> Bool
needsTypechecking (Untyped _ _) = True
needsTypechecking (Typed _ _ _) = True
needsTypechecking _             = False

isImport :: Decl -> Bool
isImport (Import _ _) = True
isImport _            = False

isAxiom :: Decl -> Bool
isAxiom (Axiom _) = True
isAxiom _         = False

isBucket :: Decl -> Bool
isBucket (Bucket _ _ _) = True
isBucket _              = False

data Bucket = Bucket_Leaf   Con
            | Bucket_Arg    UVar
            | Bucket_Bucket ErrorMsg [Bucket]
            deriving (Show, Generic)

instance Alpha Bucket
instance Subst Ty Bucket

instance Alpha Decl
instance Subst (Expr ()) Decl where
  isvar _ = Nothing
  subst v1 e1 (Untyped v2    e2) = Untyped v2 (subst v1 e1 e2)
  subst v1 e1 (Typed   v2 t2 e2) = Typed v2 t2 (subst v1 e1 e2)
  subst _  _  (Import  v2 t2)    = Import v2 t2
  subst _  _  (Axiom   ax)       = Axiom ax
  substs ss (Untyped v2    e2) = Untyped v2 (substs ss e2)
  substs ss (Typed   v2 t2 e2) = Typed v2 t2 (substs ss e2)
  substs _  (Import  v2 t2)    = Import v2 t2
  substs _  (Axiom   ax)       = Axiom ax
instance Subst Ty Decl where
  isvar _ = Nothing
  subst v1 e1 (Untyped v2    e2) = Untyped v2 (subst v1 e1 e2)
  subst v1 e1 (Typed   v2 t2 e2) = Typed v2 t2 (subst v1 e1 e2)
  subst _  _  (Import  v2 t2)    = Import v2 t2
  subst _  _  (Axiom   ax)       = Axiom ax
  substs ss (Untyped v2    e2) = Untyped v2 (substs ss e2)
  substs ss (Typed   v2 t2 e2) = Typed v2 t2 (substs ss e2)
  substs _  (Import  v2 t2)    = Import v2 t2
  substs _  (Axiom   ax)       = Axiom ax
