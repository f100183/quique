{-# language MultiParamTypeClasses #-}
{-# language FlexibleInstances #-}
{-# language FlexibleContexts #-}
{-# language PostfixOperators #-}
{-# language DeriveGeneric #-}
{-# language PatternSynonyms #-}
module Quique.Solver where

import CHR.Typed
import CHR.Untyped (Rule(..))
import Control.Monad.Extra (anyM)
import Control.Monad.State.Strict hiding (when, guard)
import Data.List (find, isPrefixOf, partition, nub, lookup, union)
import qualified Data.List as L
import Data.Maybe (catMaybes, isJust, fromJust)
import Data.TermWithVars (ssubst, Substitutable, Unifiable)
import GHC.Generics
import Unbound.Generics.LocallyNameless
import Unbound.Generics.LocallyNameless.Internal.Fold (toListOf)

import Quique.Types

import Debug.Trace

data LoopChoice = NoLoop | LoopForTheBest

data CCSolution = CCSolution { substitution :: [(TyVar, Ty)]
                             , residual     :: [CC] }

solve :: LoopChoice -> [Axiom] -> MonoVarsInfo -> ErrorConSet -> [ErrorInfo] -> CCSolution
solve lp ax ng cs einfo = runFreshM $ solve' lp ax ng cs einfo

solve' :: LoopChoice -> [Axiom] -> MonoVarsInfo -> ErrorConSet -> [ErrorInfo] -> FreshM CCSolution
solve' lp ax ng cs einfo
  = do let initialscix  = 0
           initialscope = [ScopeElt Exists initialscix]
           -- initialctx   = 0
           initialcs    = CurrentScopeIx (T initialscix)
                        : VarsScopeInfo (T [(v,initialscope) | v <- nub (fvs cs)])
                          -- Start treating all as impredicative
                          -- but on first round take these as predicative
                        : VarsCurrentStatus (T mempty)
                        : VarsNextStatus    (T ng)
                        : [Ok (T c) (T initialscope) (T ctx) | (c, ctx) <- cs]
                        ++ [CtxParent (T c1) (T c2) (T (compute_parent_number einfo c1)) | (c1, c2, _) <- einfo]
                        ++ [CtxMsg (T c) (T m) | (c, _, m) <- einfo]
           extrarules   = map axiomToRule ax
       sol <- head <$> runMonadicCHR (rules lp ++ extrarules) [] initialcs
       -- trace (show (justifications sol)) $
       return (ccToSol $ remaining sol)
  where isSubstitutionCC :: TyVarScopes -> CC -> Bool
        isSubstitutionCC var_info (OkEq (T (Ty_Var (T v))) (T _) (T s) (T _))
          | Just s' <- lookup v var_info, s == s' = True
        isSubstitutionCC _        _               = False
        obtainSubstitutionCC :: [CC] -> [(TyVar, Ty)]
        obtainSubstitutionCC = map (\(OkEq (T (Ty_Var (T v))) (T ty) _ _) -> (v, ty))
        isAssumed :: CC -> Bool
        isAssumed (Ok  _ (T scope) _)
          | ScopeElt t _ <- last scope = t == Assume
        isAssumed (Err _ (T scope) _ _)
          | ScopeElt t _ <- last scope = t == Assume
        isAssumed _ = False
        ccToSol :: [CC] -> CCSolution
        ccToSol ccs = let Just (VarsScopeInfo (T var_scope_info)) = find is_vars_scope_info ccs
                          (ss, res) = partition (isSubstitutionCC var_scope_info) ccs
                       in CCSolution (obtainSubstitutionCC ss)
                                     (filter (not . isAssumed) res)
        compute_parent_number :: [ErrorInfo] -> ErrorContextIx -> Int
        compute_parent_number es ix = case find (\(c,_,_) -> c == ix) es of
                                        Nothing      -> 0
                                        Just (_,p,_) -> 1 + compute_parent_number es p

type ScopeIx        = Int
type Priority       = Int
type TyVarScopes    = [(TyVar, Scope)]

data ScopeType = Assume | ForAll | Exists
               deriving (Eq, Show, Generic)
data ScopeElt  = ScopeElt ScopeType ScopeIx
               deriving (Eq, Show, Generic)
type Scope     = [ScopeElt]

data CC = -- The first two are the ones really dealing with constraints
          Ok  (U Con) (U Scope) (U ErrorContextIx)
        | Err (U Con) (U Scope) (U ErrorContextIx) (U (Maybe String))
          -- Counter used to extend scopes
        | CurrentScopeIx (U ScopeIx)
          -- Information about error contexts
        | CtxParent (U ErrorContextIx) {- child -} (U ErrorContextIx) {- parent -} (U Int)
        | CtxMsg    (U ErrorContextIx) (U ErrorMsg)
          -- Information about variables, scopes and guardedness
        | VarsScopeInfo (U TyVarScopes)
          -- Used to decide the status of variables
        | VarsCurrentStatus (U MonoVarsInfo)
        | VarsNextStatus    (U MonoVarsInfo)
        deriving (Show, Generic)

is_vars_scope_info :: CC -> Bool
is_vars_scope_info (VarsScopeInfo (T _)) = True
is_vars_scope_info _                     = False

instance Alpha ScopeType
instance Subst Ty ScopeType
instance Substitutable RuleVar ScopeType
instance Unifiable RuleVar     ScopeType
instance Alpha ScopeElt
instance Subst Ty ScopeElt
instance Substitutable RuleVar ScopeElt
instance Unifiable RuleVar     ScopeElt
instance Alpha CC
instance Subst Ty CC
instance Substitutable RuleVar CC
instance Unifiable RuleVar     CC

type QMonad = FreshM
type QRule = MonadicCHR QMonad Priority String Bool CC
type QPass = ExtraPass QMonad String CC

pattern OkEq    x   y s e     = Ok  (T (Con_Eq   x   y)) s e
pattern ErrEq   x   y s e m   = Err (T (Con_Eq   x   y)) s e m
pattern OkInst  x n r y s e   = Ok  (T (Con_Inst x n r y)) s e
pattern ErrInst x n r y s e m = Err (T (Con_Inst x n r y)) s e m
pattern OkGen   x n r y s e   = Ok  (T (Con_Gen  x n r y)) s e
pattern ErrGen  x n r y s e m = Err (T (Con_Gen  x n r y)) s e m

q :: QRule -> QRule
q = id

scopeVar   :: Int
scopeVar   = 11111
contextVar :: Int
contextVar = 22222

axiomToRule :: Axiom -> QRule
axiomToRule (Axiom_ConstraintImpl h bs)
  = Rule { name   = ""
         , given  = [Ok (T h) (V scopeVar) (V contextVar)]
         , wanted = []
         , guard  = const True
         , body   = \s -> return [ssubst s [Ok (T b) (V scopeVar) (V contextVar) | b <- bs]]
         , prio   = const 10 } 
axiomToRule (Axiom_ConstraintIff h bs)
  = Rule { name   = ""
         , given  = []
         , wanted = [Ok (T h) (V scopeVar) (V contextVar)]
         , guard  = const True
         , body   = \s -> return [ssubst s [Ok (T b) (V scopeVar) (V contextVar) | b <- bs]]
         , prio   = const 10 }

rules :: LoopChoice -> [QRule]
rules _ = []
{-
-- rules = []
rules lp = [ flow
           , eqrefl, tdec, tdecfailcon, tdecfailarg, occcheck
           , conforall1, conforall2
           , orivarscope, orivar, orielse
           , substi
           , instrefl
           , instmucon, instmuvar
           , instforallr
           , instforalll1con, instforalll1var
           , instforalll2con, instforalll2var
           , ctxerrfound, ctxparent, errmsg
           , floatvar, scopecheck
           , predprop1, predprop2, predpro3, predpro4
           , defpred lp
           -- , startdef, applydef1, applydef2, applydef3, pauseok, finishdef
           ] ++ case lp of
                  LoopForTheBest -> [nopredprop1, nopredprop2]
                  NoLoop         -> [killpred1, killpred2]

-- Flow of information
flow = q $ rule "flow" $ \c s1 s2 e1 e2 ->
  [Ok (c?) (s1?) (e1?)] \\ [Ok (c?) (s2?) (e2?)] <=> []
  `when` flows (s1!) (s2!)
  `priority` 1

flows :: U Scope -> U Scope -> Bool
flows _ (T s2)
  -- Things do not flow into LHS of assumptions
  | ScopeElt Assume _ <- last s2
  = False
flows (T s1) (T s2)
  = s1 `isPrefixOf` s2
flows _ _ = False

-- Equality
eqrefl = q $ rule "eqrefl" $ \x y s e ->
  [OkEq (x?) (y?) (s?) (e?)] <=> []
  `when` aeq (x#) (y#)
  `priority` 2
tdec = q $ rule "tdec" $ \f args1 args2 s e ->
  [OkEq (TTy_Con (f?) (args1?)) (TTy_Con (f?) (args2?)) (s?) (e?)]
  <=> zipWith (\a1 a2 -> (OkEq (T a1) (T a2) (s!) (e!))) (map unT (args1#)) (map unT (args2#))
  `when` length (args1#) == length (args2#)
  `priority` 4
  where unT (T t) = t
tdecfailcon = q $ rule "tdecfailcon" $ \f g args1 args2 s e ->
  [OkEq (TTy_Con (f?) (args1?)) (TTy_Con (g?) (args2?)) (s?) (e?)]
  <=> [ErrEq (TTy_Con (f!) (args1!)) (TTy_Con (g!) (args2!)) (s!) (e!) (T Nothing)]
  `when` (f#) /= (g#)
  `priority` 4
tdecfailarg = q $ rule "tdecfailarg" $ \f args1 args2 s e ->
  [OkEq (TTy_Con (f?) (args1?)) (TTy_Con (f?) (args2?)) (s?) (e?)]
  <=> [ErrEq (TTy_Con (f!) (args1!)) (TTy_Con (f!) (args2!)) (s!) (e!) (T Nothing)]
  `when` length (args1#) /= length (args2#)
  `priority` 4
occcheck = q $ rule "occcheck" $ \v t s e ->
  [OkEq (TTy_Var (v?)) (t?) (s?) (e?)]
  <=> [ErrEq (TTy_Var (v!)) (t!) (s!) (e!) (T Nothing)]
  `when` contFreshM (isfv_not_in_fams (v#) (t#)) (name2Integer (v#) + 1)
  `priority` 2
  where isfv_not_in_fams :: TyVar -> Ty -> FreshM Bool
        isfv_not_in_fams v (Ty_Var (T w))
          = return (v == w)
        isfv_not_in_fams v (Ty_Con s (T args))
          = anyM (isfv_not_in_fams' v) args
        isfv_not_in_fams v (Ty_ForAll (T b))
          = do (_, (_, ty)) <- unbind b
               isfv_not_in_fams v ty
        isfv_not_in_fams _ _
          = return False
        isfv_not_in_fams' :: TyVar -> U Ty -> FreshM Bool
        isfv_not_in_fams' v (T t) = isfv_not_in_fams v t
        isfv_not_in_fams' v _     = error "Metavars not allowed here"
-- Constructor and forall fail
conforall1 = q $ rule "conforall1" $ \f args b s e ->
  [OkEq (TTy_Con (f?) (args?)) (TTy_ForAll (b?)) (s?) (e?)]
  <=> [ErrEq (TTy_Con (f!) (args!)) (TTy_ForAll (b!)) (s!) (e!) (T Nothing)]
  `priority` 1
conforall2 = q $ rule "conforall2" $ \f args b s e ->
  [OkEq (TTy_ForAll (b?)) (TTy_Con (f?) (args?)) (s?) (e?)]
  <=> [ErrEq (TTy_Con (f!) (args!)) (TTy_ForAll (b!)) (s!) (e!) (T Nothing)]
  `priority` 1
-- Predicative variables and forall fail
-- REMOVE IF LOOPING DEFAULTING IS USED!!!

-- Orientation
orivarscope = q $ rule "orivar" $ \v w s e vinfo ->
  [VarsScopeInfo (vinfo?)] \\ [OkEq (TTy_Var (v?)) (TTy_Var (w?)) (s?) (e?)]
  <=> [OkEq (TTy_Var (w!)) (TTy_Var (v!)) (s!) (e!)]
  `when` (let Just s1 = lookup (v#) (vinfo#)
              Just s2 = lookup (w#) (vinfo#)
           in s1 /= s2 && flows (T s1) (T s2))
  `priority` 1
orivar = q $ rule "orivar" $ \v w s e vinfo ->
  [VarsScopeInfo (vinfo?)] \\ [OkEq (TTy_Var (v?)) (TTy_Var (w?)) (s?) (e?)]
  <=> [OkEq (TTy_Var (w!)) (TTy_Var (v!)) (s!) (e!)]
  `when` (let Just s1 = lookup (v#) (vinfo#)
              Just s2 = lookup (w#) (vinfo#)
           in s1 == s2 && (w#) < (v#))
  `priority` 1
orielse = q $ rule "orielse" $ \v t s e ->
  [OkEq (t?) (TTy_Var (v?)) (s?) (e?)]
  <=> [OkEq (TTy_Var (v!)) (t!) (s!) (e!)]
  `when` not (isavar (t!))
  `priority` 1
  where isavar :: U Ty -> Bool
        isavar (TTy_Var _) = True
        isavar _           = False

-- Prevent propagation if equated to a closed element
nopredprop1 = q $ rule "nopredprop1" $ \v1 t2 s e ng ->
  [OkEq (TTy_Var (v1?)) (TTy_ForAll (t2?)) (s?) (e?)]
  \\ [VarsCurrentStatus (ng?)]
  <=> (let newNng = liftToMonoVarsInfo (L.\\ [(v1#)]) (ng#)
       in [VarsCurrentStatus (T newNng)])
  `when` (v1#) `isTopLevelMonoIn` (ng#)
  `priority` 0
nopredprop2 = q $ rule "nopredprop2" $ \v1 t2 s e ng ->
  [OkEq (TTy_ForAll (t2?)) (TTy_Var (v1?)) (s?) (e?)]
  \\ [VarsCurrentStatus (ng?)]
  <=> (let newNng = liftToMonoVarsInfo (L.\\ [(v1#)]) (ng#)
       in [VarsCurrentStatus (T newNng)])
  `when` (v1#) `isTopLevelMonoIn` (ng#)
  `priority` 0

-- Do not allow equating monomorphic variables to forall types
killpred1 = q $ rule "killpred1" $ \v1 t2 s e ng ->
  [VarsCurrentStatus (ng?)]
  \\ [OkEq (TTy_Var (v1?)) (TTy_ForAll (t2?)) (s?) (e?)]
  <=> [ErrEq (TTy_Var (v1!)) (TTy_ForAll (t2!)) (s!) (e!) (T Nothing)]
  `when` (v1#) `isTopLevelMonoIn` (ng#)
  `priority` 0
killpred2 = q $ rule "killpred2" $ \v1 t2 s e ng ->
  [VarsCurrentStatus (ng?)]
  \\ [OkEq (TTy_ForAll (t2?)) (TTy_Var (v1?)) (s?) (e?)]
  <=> [ErrEq (TTy_Var (v1!)) (TTy_ForAll (t2!)) (s!) (e!) (T Nothing)]
  `when` (v1#) `isTopLevelMonoIn` (ng#)
  `priority` 0

-- Fully monomorphic propagation
predprop1 = q $ rule "predpop1" $ \v1 t2 s e ng ->
  [OkEq (TTy_Var (v1?)) (t2?) (s?) (e?)]
  \\ [VarsCurrentStatus (ng?)]
  <=> [VarsCurrentStatus (T ((ng#) `unionFullyMono` nub (fvs (t2#)))) ]
  `when` ((v1#) `elem` (fullyMono (ng#)) && not (null (nub (fvs (t2#)) L.\\ (fullyMono (ng#)))))
  `priority` 1
predprop2 = q $ rule "predpop2" $ \t1 v2 s e ng ->
  [OkEq (t1?) (TTy_Var (v2?)) (s?) (e?)]
  \\ [VarsCurrentStatus (ng?)]
  <=> [VarsCurrentStatus (T ((ng#) `unionFullyMono` nub (fvs (t1#)))) ]
  `when` ((v2#) `elem` (fullyMono (ng#)) && not (null (nub (fvs (t1#)) L.\\ (fullyMono (ng#)))))
  `priority` 1
-- Top-level monomorphic propagation
predpro3 = q $ rule "predprop3" $ \v1 v2 s e ng ->
  [OkEq (TTy_Var (v1?)) (TTy_Var (v2?)) (s?) (e?)]
  \\ [VarsCurrentStatus (ng?)]
  <=> [VarsCurrentStatus (T ((ng#) `plusTopLevelMono` (v2#)))]
  `when` ((v1#) `elem` (topLevelMono (ng#)) && not ((v2#) `isTopLevelMonoIn` (ng#)))
  `priority` 1
predpro4 = q $ rule "predprop4" $ \v1 v2 s e ng ->
  [OkEq (TTy_Var (v2?)) (TTy_Var (v1?)) (s?) (e?)]
  \\ [VarsCurrentStatus (ng?)]
  <=> [VarsCurrentStatus (T ((ng#) `plusTopLevelMono` (v2#)))]
  `when` ((v1#) `elem` (topLevelMono (ng#)) && not ((v2#) `isTopLevelMonoIn` (ng#)))
  `priority` 1

-- Substitution
substi = q $ rule "subst" $ \v t s1 c s2 e ->
  [OkEq (TTy_Var (v?)) (t?) (s1?) (e?)] \\ [Ok (c?) (s2?) (e?)]
  <=> [Ok (T (subst (v#) (t#) (c#))) (s2!) (e!)]
  `when` flows (s1!) (s2!) && elem (v#) (toListOf fv (c#))
  `priority` 2

-- Instantiation
instrefl = q $ rule "instrefl" $ \x n y s e ->
  [OkInst (TGenTy_Sigma (x?)) (n?) (y?) (s?) (e?)] <=> []
  `when` aeq (x#) (y#)
  `priority` 2

-- Version only sound w/o fake polymorphism
instmucon = q $ rule "instmucon" $ \f args1 n t s e ->
  [OkInst (TGenTy_Sigma (TTy_Con (f?) (args1?))) (n?) (t?) (s?) (e?)]
  <=> [OkEq (TTy_Con (f!) (args1!)) (t!) (s!) (e!)]
  `priority` 3
instmuvar = q $ rule "instmuvar" $ \v n t s e ng ->
  [VarsCurrentStatus (ng?)]
  \\ [OkInst (TGenTy_Sigma (TTy_Var (v?))) (n?) (t?) (s?) (e?)]
  <=> [OkEq (TTy_Var (v!)) (t!) (s!) (e!)]
  `when` ((v#) `isTopLevelMonoIn` (ng#))
  `priority` 3

instforallr = q $ rule "instforallr" $ \t n b s ctx ix vinfo ng ->
  [ OkInst (t?) (n?) (TTy_ForAll (b?)) (s?) (ctx?), CurrentScopeIx (ix?)
  , VarsScopeInfo (vinfo?), VarsCurrentStatus (ng?) ]
  <=!> (do (vs, (cs, ty)) <- unbind (b#)
           let initial_scope = (s#)
               univ_scope = initial_scope ++ [ScopeElt ForAll (ix#)]
               impl_scope = univ_scope ++ [ScopeElt Assume (ix#)]
               exis_scope = impl_scope ++ [ScopeElt Exists (ix#)]
           let tyeq = OkInst (t!) (n!) (T ty) (T exis_scope) (ctx!)
               css = [Ok (T c) (T impl_scope) (ctx!) | c <- cs]
               newvinfo = [(v, univ_scope) | v <- vs]
           -- We need to update the current scope
           return ( CurrentScopeIx (T ((ix#) + 1))
                  : VarsScopeInfo (T (newvinfo `union` (vinfo#)))
                    -- Skolems are always taken as predicative
                  : VarsCurrentStatus (T (unionFullyMono (ng#) vs))
                  : tyeq : css ))
  `priority` 3

instforalll1con = q $ rule "instforalll1con" $ \b n f args s e vinfo nng ->
  [ OkInst (TGenTy_Vars (b?)) (n?) (TTy_Con (f?) (args?)) (s?) (e?)
  , VarsScopeInfo (vinfo?), VarsNextStatus (nng?) ]
  <=!> perform_instantiation_error_ix b n (TTy_Con (f!) (args!)) s e vinfo nng
  `priority` 3
instforalll1var = q $ rule "instforalll1var" $ \b n v s e ng vinfo nng ->
  [VarsCurrentStatus (ng?)]
  \\ [ OkInst (TGenTy_Vars (b?)) (n?) (TTy_Var (v?)) (s?) (e?)
     , VarsScopeInfo (vinfo?), VarsNextStatus (nng?) ]
  <=!> perform_instantiation_error_ix b n (TTy_Var (v!)) s e vinfo nng
  `when` ((v#) `isTopLevelMonoIn` (ng#))
  `priority` 3
-- In this case guardedness information is part of generalization
perform_instantiation_error_ix b1 n ty2 s e vinfo nng
  = do (vs, (newNg, cs, ty)) <- unbind (b1#)
       let tyeq = OkInst (TGenTy_Sigma (T ty)) (n!) ty2 (s!) (e!)
           css  = [Ok (T c) (s!) (T eix) | (c, eix) <- cs]
           newvinfo = [(v, (s#)) | v <- vs]
       return ( VarsScopeInfo (T (newvinfo `union` (vinfo#)))
              : VarsNextStatus (T ((nng#) `mappend` newNg))
              : tyeq : css)

instforalll2con = q $ rule "instforalll2con" $ \b n f args s e vinfo nng ->
  [ OkInst (TGenTy_Sigma (TTy_ForAll (b?))) (n?) (TTy_Con (f?) (args?)) (s?) (e?)
  , VarsScopeInfo (vinfo?), VarsNextStatus (nng?) ]
  <=!> perform_instantiation b n (TTy_Con (f!) (args!)) s e vinfo nng
  `priority` 3
instforalll2var = q $ rule "instforalll2var" $ \b n v s e ng vinfo nng ->
  [VarsCurrentStatus (ng?)]
  \\ [ OkInst (TGenTy_Sigma (TTy_ForAll (b?))) (n?) (TTy_Var (v?)) (s?) (e?)
     , VarsScopeInfo (vinfo?), VarsNextStatus (nng?) ]
  <=!> perform_instantiation b n (TTy_Var (v!)) s e vinfo nng
  `when` ((v#) `isTopLevelMonoIn` (ng#))
  `priority` 3
perform_instantiation b1 n ty2 s e vinfo nng
  = do (vs, (cs, ty)) <- unbind (b1#)
           -- The tricky part: decide about guardedness
           -- 1. Look at the first n elements in the arrow
       let (elts, _) = splitArrow (n#) (T ty)
           -- 2. Take those elements in the args
           argsV = vs `L.intersect` fvs elts
           -- 3. From those, decide whether they are top-level
           (impredV, topLevelV) = partition (is_guarded_var elts) argsV
           -- 4. The rest are fully-mono
           fullyV = vs L.\\ argsV
           -- 5. Done
           newVars = MonoVarsInfo fullyV (topLevelV L.\\ fullyV)
       let tyeq = OkInst (TGenTy_Sigma (T ty)) (T 0) ty2 (s!) (e!)
           css  = [Ok (T c) (s!) (e!) | c <- cs]
           newvinfo = [(v, (s#)) | v <- vs]
       return ( VarsScopeInfo (T (newvinfo `union` (vinfo#)))
              : (VarsNextStatus (T ((nng#) `mappend` newVars)))
              : tyeq : css )

-- VERSION 1: NOTHING IS GUARDED
-- is_guarded_var _ _ = False
-- VERSION 2: REAL GUARDEDNESS
is_guarded_var elts v
  = flip any elts $ \elt ->
      case elt of 
        TTy_Var _ -> False
        _         -> v `elem` fvs elt

-- Error contexts: right before 10000
ctxerrfound = q $ rule "ctxerrfound" $ \ctx msg c s ->
  [CtxMsg (ctx?) (msg?)] \\ [Err (c?) (s?) (ctx?) (T Nothing)]
  <=> [Err (c!) (s!) (ctx!) (T (Just (msg#)))]
  `priority` 1
ctxparent = q $ rule "ctxparent" $ \ctx parent n c s ->
  [CtxParent (ctx?) (parent?) (n?)] \\ [Ok (c?) (s?) (ctx?)]
  <=> [Ok (c!) (s!) (parent!)]
  `priority` 10000 - (n#)
errmsg = q $ rule "errmsg" $ \msg s e ->
  [Ok (T (Con_Err (msg?))) (s?) (e?)]
  <=> [Err (T (Con_Err (msg!))) (s!) (e!) (T (Just (msg#)))]
  `priority` 1

-- Floating: try it after all error contexts have been dispatched
floatvar = q $ rule "floatvar" $ \v1 t2 s e vinfo ->
  [VarsScopeInfo (vinfo?)] \\ [OkEq (TTy_Var (v1?)) (t2?) (s?) (e?)]
  <=> [OkEq (TTy_Var (v1!)) (t2!) (T (fromJust (float_scope v1 t2 s vinfo))) (e!)]
  `when` isJust (float_scope v1 t2 s vinfo)
  `priority` 10001

float_scope v1 t2 s vinfo = float_scope_ (v1#) (t2#) (s#) (vinfo#)
  where float_scope_ vv tt ss vvinfo
          | Just s' <- lookup vv vvinfo, s' /= ss
          -- Can be floated if introduced as existential
          , ScopeElt Exists _ <- last s'
          -- And all used variables are in outer scopes
          , all (\v -> T (fromJust (lookup v vvinfo)) `flows` T s') (fvs tt)
          = Just s'
        float_scope_ _ _ _ _
          = Nothing

-- Defaulting: move the newly introduced variables to its current status
defpred lp = q $ rule "defpred" $ \t1 n t2 s e ng nng ->
  [OkInst (t1?) (n?) (t2?) (s?) (e?)]  -- if there is some instantiation missing
  \\ [VarsCurrentStatus (ng?), VarsNextStatus (nng?)]
  <=> (let allNng = (ng#) `mappend` (nng#)
        in [VarsCurrentStatus (T allNng), VarsNextStatus (T mempty)])
  `when` not (null (fullyMono (nng#)) && null (topLevelMono (nng#)))
  `priority` lpPriority lp
             -- smaller number == higher priority
  where lpPriority LoopForTheBest = 10002
        lpPriority NoLoop         = -10002

-- Var scope check: start at the very end
scopecheck = q $ rule "scopecheck" $ \v1 t2 s e vinfo ->
  [VarsScopeInfo (vinfo?)] \\ [OkEq (TTy_Var (v1?)) (t2?) (s?) (e?)]
  <=> [ErrEq (TTy_Var (v1!)) (t2!) (s!) (e!) (T Nothing)]
  `when` (let Just s' = lookup (v1#) (vinfo#)
           in s' /= (s#) && T s' `flows` (s!))
  `priority` 10003


{- OLD DEFAULTING STARTEGY!!
-- Defaulting: start at level 10001
startdef = q $ rule "startdef" $ \t1 t2 s e ->
  [OkInst (t1?) (t2?) (s?) (e?)]
  ==> [InDefaulting]
  `priority` 10001  -- The first of extra phases
-- These rules have the highest priority so that
-- they are applied all when possible
applydef1 = q $ rule "applydef1" $ \b1 t2 s e vinfo ->
  [InDefaulting] \\ [OkInst (TGenTy_Vars (b1?)) (t2?) (s?) (e?), VarsScopeInfo (vinfo?)]
  <=!> perform_instantiation_error_ix OkInst Pause b1 (t2!) s e vinfo
  `priority` (-2)
applydef2 = q $ rule "applydef2" $ \b1 t2 s e vinfo ->
  [InDefaulting] \\ [OkInst (TGenTy_Sigma (TTy_ForAll (b1?))) (t2?) (s?) (e?), VarsScopeInfo (vinfo?)]
  <=!> perform_instantiation PauseInst Pause b1 (t2!) s e vinfo
  `priority` (-2)
applydef3 = q $ rule "applydef3" $ \v1 t2 s e ->
  [InDefaulting] \\ [OkInst (TGenTy_Sigma (TTy_Var (v1?))) (t2?) (s?) (e?)]
  <=> [OkEq (T (Ty_Var (v1!))) (t2!) (s!) (e!)]
  `priority` (-2)
-- When applying defaulting, we want to do only
-- one pass at a time. To model this, instead of Ok
-- we generate Pause, which are then turned into
-- Ok in a later phase.
pauseok = q $ rule "pauseok" $ \c s e ->
  [Pause (c?) (s?) (e?)] <=> [Ok (c!) (s!) (e!)] `priority` 0
-- Once we are finished with all defaulting (in priority -2)
-- we just remove the tag which told us so (in priority -1)
finishdef = q $ rule "finishdef" $
  [InDefaulting] <=> [] `priority` (-1)
-}
-}